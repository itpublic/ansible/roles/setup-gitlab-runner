# setup-gitlab-runner
Easy setup of gitlab runners using Ansible.

Install on command line with
```
ansible-galaxy install git+https://git.app.uib.no/itpublic/ansible/roles/setup-gitlab-runner.git
```
or include in ``requirements.yml`` to install with `ansible-galaxy install -fr roles/requirements.yml`.
```
# roles/requirements.yml
roles:
  - src: git+https://git.app.uib.no/itpublic/ansible/roles/setup-gitlab-runner.git
    version: 1.0.0
```
## Role variables
Default variables are set in defaults/main.yml.

Place these variables in your `group_vars/<runner_name>`-file.

    runner_name: "<runner_name>"
    registration_token: "<token>"
    gitlab_url: "https://git.app.uib.no/"

Optional

    tags:  # You can set up jobs to only use Runners with specific tags. ex: tags: "docker,shell"
    request_concurrency: "2"  # How many requests can a runner handle from the Gitlab CI job queue
    cron_mailto: <mail_adr> # mailto for docker prune cron job, if docker_prune: true
    authorized_users:
      - username: <username>
        key: <ssh-key>

## Example Playbook

```
---
- hosts: "{{ runner | default('all') }}"
  become: true
  tasks:
    - include_role:
        name: setup-gitlab-runner


